package u05lab.code

import java.lang.IllegalArgumentException

object beginTest {

  var erf: ExamResultFactory = ExamResultFactoryImpl()
  var em: ExamsManager = ExamsManagerImpl()

  def begin(): Unit = {

    erf = ExamResultFactoryImpl()
    em = ExamsManagerImpl()
    em.createNewCall("gennaio")
    em.createNewCall("febbraio")
    em.createNewCall("marzo")
    em.addStudentResult("gennaio", "rossi", erf.failed) // rossi -> fallito
    em.addStudentResult("gennaio", "bianchi", erf.retired) // bianchi -> ritirato
    em.addStudentResult("gennaio", "verdi", erf.succeeded(28)) // verdi -> 28
    em.addStudentResult("gennaio", "neri", erf.succeededCumLaude) // neri -> 30L
    em.addStudentResult("febbraio", "rossi", erf.failed) // etc..
    em.addStudentResult("febbraio", "bianchi", erf.succeeded(20))
    em.addStudentResult("febbraio", "verdi", erf.succeeded(30))
    em.addStudentResult("marzo", "rossi", erf.succeeded(25))
    em.addStudentResult("marzo", "bianchi", erf.succeeded(25))
    em.addStudentResult("marzo", "viola", erf.failed)
  }

}

object ExamsResultFactoryTest extends App {

  /* See: https://bitbucket.org/mviroli/oop2018-esami/src/master/a01b/e1/Test.java */

  import beginTest._
  // esame fallito, non c'è voto

  println("Exam kind: " + erf.failed.kind) // FAILED
  println("There isn't vote: " + erf.failed.evaluation.isEmpty) //True
  println("No laude: " + erf.failed.cumLaude) //FALSE
  println("Exam: " + erf.failed) //FAILED

  //lo studente si è ritirato, non c'è voto
  println("Exam kind: " + erf.retired.kind) //RETIRED
  println("No vote because of the retire: " + erf.retired.evaluation.isEmpty) //True
  println("No laude: " + erf.retired.cumLaude)
  println("Exam: " + erf.retired)

  //30L
  println("Succeed cum laude: " + erf.succeededCumLaude.kind) //SUCCEED
  println("Vote: " + erf.succeededCumLaude.evaluation.get) //30
  println("Laude: " + erf.succeededCumLaude.cumLaude) //True
  println("Exam: " + erf.succeededCumLaude) //SUCCEED(30L)

  //esame superato, ma non con lode
  println("Succeed: " + erf.succeeded(28).kind) //SUCCEED
  println("Vote: " + erf.succeeded(28).evaluation) //28
  println("Laude: " + erf.succeeded(28).cumLaude) //false
  println("Exam: " + erf.succeeded(28)) //SUCCEED(28)

  //verifica eccezione in ExamResultFactory: voto > 30
  try {
    val vote = erf.succeeded(32)
  } catch {
    case e: Exception => println(e  + " launched: vote can't be a number bigger than 30")
  }

  //verifica eccezione in ExamResultFactory: voto < 18
  try {
    val vote = erf.succeeded(17)
  } catch {
    case e: Exception => println(e  + " launched: vote can't be a number lesser than 18")
  }

}

object ExamsManagerTest extends App {

  import beginTest._

  begin()
  // partecipanti agli appelli di gennaio e marzo
  println(em.getAllStudentFromCall("gennaio")) //Set(rossi, verdi, bianchi, neri)
  println(em.getAllStudentFromCall("marzo")) //Set(viola, rossi, bianchi)

  // promossi di gennaio con voto
  println(em.getEvaluationsMapFromCall("gennaio").size)
  println(em.getEvaluationsMapFromCall("gennaio").get("verdi").get)
  println(em.getEvaluationsMapFromCall("gennaio").get("neri").get)

  // promossi di febbraio con voto
  println(em.getEvaluationsMapFromCall("febbraio").size)
  println(em.getEvaluationsMapFromCall("febbraio").get("bianchi").get)
  println(em.getEvaluationsMapFromCall("febbraio").get("verdi").get)

  // tutti i risultati di rossi (attenzione ai toString!!)
  println(em.getResultsMapFromStudent("rossi").size)
  println(em.getResultsMapFromStudent("rossi").get("gennaio"))
  println(em.getResultsMapFromStudent("rossi").get("febbraio"))
  println(em.getResultsMapFromStudent("rossi").get("marzo"))

  // tutti i risultati di bianchi
  println(em.getResultsMapFromStudent("bianchi").size)
  println(em.getResultsMapFromStudent("bianchi").get("gennaio"))
  println(em.getResultsMapFromStudent("bianchi").get("febbraio"))
  println(em.getResultsMapFromStudent("bianchi").get("marzo"))

  // tutti i risultati di neri
  println(em.getResultsMapFromStudent("neri").size)
  println(em.getResultsMapFromStudent("neri").get("gennaio"))
}

object testExamsManagement extends App {

  import beginTest._

  begin()
  println(em.getBestResultFromStudent("rossi"))
  println(em.getBestResultFromStudent("bianchi"))
  println(em.getBestResultFromStudent("neri"))
  println(em.getBestResultFromStudent("viola"))

  try {
    em.createNewCall("marzo")
  } catch {
    case e: IllegalArgumentException => println("Call present yet")
  }

  try {
    em.addStudentResult("gennaio", "verdi", erf.failed)
  } catch {
    case e: IllegalArgumentException => println("Student can't substain two exams during the same call")
  }

}