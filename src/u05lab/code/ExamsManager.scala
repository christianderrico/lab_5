package u05lab.code

import scala.collection.mutable

sealed trait ExamsManager {

  def exams: mutable.Map[String, mutable.Map[String, ExamResult]]

  private val checkCondition: Boolean => Unit = {
    case true =>
    case _ => throw new IllegalArgumentException
  }

  def createNewCall(call: String):Unit = {
    checkCondition(!exams.contains(call))
    exams.put(call, mutable.Map.empty)
  }

  def addStudentResult(call: String, student: String, result: ExamResult): Option[ExamResult] = {
    checkCondition(exams.contains(call) && !exams(call).contains(student))
    exams(call).put(student, result)
  }

  def getAllStudentFromCall(call: String): Set[String] = {
    checkCondition(exams.contains(call))
    exams(call).keySet.toSet
  }

  def getEvaluationsMapFromCall(call: String): Map[String, Int] = {
    checkCondition(exams.contains(call))
    Map from exams(call) filter (e => e._2.evaluation.isDefined) map {case (key, value) => (key, value.evaluation.get)}
  }

  private def filterExam(student: String): Map[String, ExamResult] = {
    Map from exams.filter(e => e._2.contains(student)) collect {case (key, value) => (key, value(student))}
  }

  def getResultsMapFromStudent(student: String): Map[String, String] =
    filterExam(student).map(k => (k._1, k._2.toString))

  def getBestResultFromStudent(student: String): Option[Int] =
    filterExam(student).values.toList.map(e => e.evaluation).max
}

case class ExamsManagerImpl(override val exams: mutable.Map[String, mutable.Map[String, ExamResult]] = mutable.Map.empty) extends ExamsManager