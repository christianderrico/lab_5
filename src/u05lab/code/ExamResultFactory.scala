package u05lab.code
import u05lab.code.ExamResult

trait ExamResultFactory {

  def failed: ExamResult

  def retired: ExamResult

  def succeededCumLaude: ExamResult

  def succeeded(evaluation: Int): ExamResult

}

case class ExamResultFactoryImpl() extends ExamResultFactory {

  private val MAX_EVALUATION: Int = 30

  override def failed: ExamResult = ExamResultImpl(Kind.failed(), Option.empty, cumLaude = false)

  override def retired: ExamResult = ExamResultImpl(Kind.retired(), Option.empty, cumLaude = false)

  override def succeededCumLaude: ExamResult = ExamResultImpl(Kind.succeed(), Some(MAX_EVALUATION), cumLaude = true)

  override def succeeded(eval: Int): ExamResult = eval match {
    case eval if eval >= 18 && eval <= 30 => ExamResultImpl(Kind.succeed(), Some(eval), cumLaude = false)
    case _ => throw new IllegalArgumentException
  }

}
