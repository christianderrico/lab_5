package u05lab.code

sealed trait Kind

object Kind {
  case class succeed() extends Kind { override def toString: String = "SUCCEED"}
  case class retired() extends Kind { override def toString: String = "RETIRED"}
  case class failed() extends Kind {  override def toString: String = "FAILED"}
}

trait ExamResult {

  def kind: Kind

  def evaluation: Option[Int]

  def cumLaude: Boolean

}

case class ExamResultImpl(override val kind: Kind,
                          override val evaluation: Option[Int],
                          override val cumLaude: Boolean) extends ExamResult {

  override def toString: String = kind match {
    case Kind.succeed() if cumLaude => "SUCCEED(30L)"
    case Kind.succeed() => "SUCCEED(" + evaluation.get + ")"
    case _ => kind.toString
  }

}