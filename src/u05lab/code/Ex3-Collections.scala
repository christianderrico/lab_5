package u05lab.code

import java.util.concurrent.TimeUnit

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.duration.FiniteDuration
import scala.util.Random

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}

object TestingUtils {

  private val elemToFind = 10

  def readAllElements(coll: Iterable[_]) = {
    coll foreach(a => {})
  }

  def getSingleElement(value: Iterable[_]): Int = value match {
    case seq: Seq[Int] => seq(Random.nextInt(seq.size))
    case buffer: mutable.Buffer[Int] => buffer(Random.nextInt(buffer.size))
    case set: Set[Int] => set.find(_ == elemToFind).get
    case mutableSet: mutable.Set[Int] => mutableSet.find(_ == elemToFind).get
    case map: Map[Int, Int] => map(elemToFind)
    case mutableMap: mutable.Map[Int, Int] => mutableMap(elemToFind)
  }

  def doubleUpAllElements(coll: Iterable[_]) = coll match {
    case seq: Seq[Int] => for(id <- Range(0, seq.size - 1)) seq.updated(id, seq(id) * 2)
    case buffer: mutable.Buffer[Int] => for(i <- buffer.indices) buffer.update(i, buffer(i) * 2)
    case set: Set[Int] => set.toList.foreach(e => set + e*2)
    case mutableSet: mutable.Set[Int] => mutableSet.foreach(e => {mutableSet -= e; mutableSet += e*2})
    case map: Map[Int, Int] => for (i <- map.keySet) map.updated(i, Random.nextInt())
    case mutableMap: mutable.Map[Int, Int] => for(i <- mutableMap.keySet) mutableMap.update(i, Random.nextInt())
  }

  def deleteThreeRandomElements(coll: Iterable[_]) = {
    for(i <- Range(0, 3)) coll.drop(Random.nextInt(coll.size))
  }

  def updateRandomlySingleElement(collection: Iterable[_]) = collection match {
    case seq: Seq[Int] => seq.updated(Random.nextInt(seq.size), Random.nextInt())
    case buffer: mutable.Buffer[Int] => buffer.update(Random.nextInt(buffer.size), Random.nextInt())
    case set: Set[Int] => set + Random.nextInt()
    case mutableSet: mutable.Set[Int] => mutableSet.update(Random.nextInt(mutableSet.size), included=true)
    case map: Map[Int, Int] => map.updated(Random.nextInt(), Random.nextInt())
    case mutableMap: mutable.Map[Int, Int] => mutableMap.update(Random.nextInt(mutableMap.size), Random.nextInt())
  }

  def testPerformance(collection: Iterable[_])(collType: String) = {
    import PerformanceUtils._
    val measures: mutable.Map[String, MeasurementResults[Unit]] = mutable.Map()
    measures += (collType + " - ReadAll" -> measure("Read all elements: " + collType){readAllElements(collection)})
    measures += (collType + " - ReadSingle" -> measure("Read single element: " + collType){getSingleElement(collection)})
    measures += (collType + " - UpdateAll" -> measure("Update all elements: " + collType){doubleUpAllElements(collection)})
    measures += (collType + " - UpdateSingle" -> measure("Update single element: " + collType){updateRandomlySingleElement(collection)})
    measures += (collType + " - Delete" -> measure("Delete: " + collType){deleteThreeRandomElements(collection)})
  }

}

object CollectionsTest extends App {

  import TestingUtils._
  /* Linear sequences: List, ListBuffer */

  val lst = (1 to 10_000).toList
  val buffer = ListBuffer from (1 to 10_000)
  val vec = Vector from (1 to 10_000)
  val arrayBuffer = ArrayBuffer from (1 to 10_000)
  private val mandatoryElem = 10
  val set = (Set from (0 to 10_000)) + mandatoryElem
  val mutableSet = (mutable.Set from(0 to 10_000)) + mandatoryElem
  val immutableMap = (Map from (0 to 10_000).zip(lst)) + (mandatoryElem -> Random.nextInt())
  val mutableMap = (mutable.Map from (0 to 10_000).zip(lst)) + (mandatoryElem -> Random.nextInt())

  val lstMeasures = testPerformance(lst)("List")
  val listBufferMeasures = testPerformance(buffer)("ListBuffer")
  val vectorMeasures = testPerformance(vec)("Vector")
  val arrayBufferMeasures = testPerformance(arrayBuffer)("ArrayBuffer")
  val immutableSetMeasures = testPerformance(set)("Set")
  val mutableSetMeasures = testPerformance(mutableSet)("MutableSet")
  val immutableMapMeasures = testPerformance(immutableMap)("ImmutableMap")
  val mutableMapMeasures = testPerformance(mutableMap)("MutableMap")

  /* Comparison */
  assert(listBufferMeasures("ListBuffer - ReadAll") < lstMeasures("List - ReadAll"))
  //Lettura su vector indicizzata
  assert(vectorMeasures("Vector - ReadAll") < lstMeasures("List - ReadAll"))
  //Update su mutable è più veloce
  assert(listBufferMeasures("ListBuffer - UpdateAll") < lstMeasures("List - UpdateAll"))
  assert(arrayBufferMeasures("ArrayBuffer - UpdateAll") < vectorMeasures("Vector - UpdateAll"))
  assert(mutableSetMeasures("MutableSet - UpdateAll") < immutableSetMeasures("Set - UpdateAll"))
  assert(mutableMapMeasures("MutableMap - UpdateAll") < immutableMapMeasures("ImmutableMap - UpdateAll"))

  
}